/*
 * extInt.cpp
 *
 *  Created on: 10 de out de 2018
 *      Author: guh86
 */

#include "extInt.h"

extInt::extInt(int_id _id, isc_t _int_config, toFuncao_t pCallback){
	this->_id = _id;
	if(_id <= 3){
		EICRA &= ~(3 << (_id*2));
		EICRA |= (_int_config << (_id*2));
	}else{
		EICRB &= ~(3 << ((_id % 4)*2));
		EICRB |= (_int_config << ((_id % 4)*2));
	}
	switch(_id){
	case 0:
		this->_pCallback_0 = pCallback;
		break;
	case 1:
		this->_pCallback_1 = pCallback;
		break;
	case 2:
		this->_pCallback_2 = pCallback;
		break;
	case 3:
		this->_pCallback_3 = pCallback;
		break;
	case 4:
		this->_pCallback_4 = pCallback;
		break;
	case 5:
		this->_pCallback_5 = pCallback;
		break;
	case 6:
		this->_pCallback_6 = pCallback;
		break;
	case 7:
		this->_pCallback_7 = pCallback;
		break;
	}
}

extInt::~extInt(){}

void extInt::enable(){
	EIMSK |= (1<<_id);//habilita a interrupcao
}

void extInt::disable(){
	EIMSK &= ~(1<<_id);//desabilita a interrupcao
}

ISR(INT0_vect){ extInt::int0_handler(); }
ISR(INT1_vect){ extInt::int1_handler(); }
ISR(INT2_vect){ extInt::int2_handler(); }
ISR(INT3_vect){ extInt::int3_handler(); }
ISR(INT4_vect){ extInt::int4_handler(); }
ISR(INT5_vect){ extInt::int5_handler(); }
ISR(INT6_vect){ extInt::int6_handler(); }
ISR(INT7_vect){ extInt::int7_handler(); }
