#include <avr/interrupt.h>
#include "extInt.h"
#include "UART.h"
#include "GPIO.h"
#include "PCInt.h"


UART uart(9600, UART::databit_8, UART::none, UART::two, UART::slow);
GPIO led1(10,GPIO::OUTPUT);

void int0_handler(void){
	uart.put('0');
	led1.toggle();
	return;
}


void PCint0_handler(void){
	uart.put('A');
	//led1.toggle();
	return;
}

void PCint1_handler(void){
	uart.put('B');
	led1.toggle();
	return;
}
void PCint2_handler(void){
	//uart.put('C');
	led1.toggle();
	return;
}


toFuncao_t extInt::_pCallback_0;
toFuncao_t extInt::_pCallback_1;
toFuncao_t extInt::_pCallback_2;
toFuncao_t extInt::_pCallback_3;
toFuncao_t extInt::_pCallback_4;
toFuncao_t extInt::_pCallback_5;
toFuncao_t extInt::_pCallback_6;
toFuncao_t extInt::_pCallback_7;

toFuncao_t PCInt::_pCallback[24];
uint8_t PCInt::_flag0;
uint8_t PCInt::_flag1;
uint8_t PCInt::_flag2;

int main( void ){


	//extInt int0_obj(extInt::INT_5, extInt::rising, &int0_handler);
	/*PCInt pcint0_obj(PCInt::PCINT_0, &PCint0_handler);
	PCInt pcint1_obj(PCInt::PCINT_16, &PCint1_handler);*/
	PCInt pcint2_obj(PCInt::PCINT_8, &PCint2_handler);
	//int0_obj.enable();
	/*pcint0_obj.enable();
	pcint1_obj.enable();*/
	pcint2_obj.enable();
	UCSR0B =0;
//	UCSR2B =0;
//	UCSR1B =0;
	sei();
	//int a = 0;
	while(1){                                   /* forever */
		if (uart.hasdata()){
			uart.dado_get();
			uart.put(PCMSK0);
			uart.put(PCMSK1);
			uart.put(PCMSK2);
		}
		pcint2_obj.manager_handler();
	}

	return 0;
}
