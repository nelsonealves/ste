/*
 * PCInt.cpp
 *
 *  Created on: 5 de nov de 2018
 *      Author: guh86
 */
#include "PCInt.h"

PCInt::PCInt(int_id id, toFuncao_t pCallback){
	this->_id = id; //salva o id
	this->tmp_id = id % 8; //salva o pino
	_pCallback[id] = pCallback; //salva a fun��o dentro do vetor de ponteiros
	if((id >= 0) & (id <= 7)){ //seta que algum dos pinos de PB foi criado
		PCICR |= (1<<PCIE0);
	}else if((id >= 8) & (id <= 15)){//seta que algum dos pinos de PJ foi criado
		PCICR |= (1<<PCIE1);
	}else if((id >= 16) & (id <= 23)){//seta que algum dos pinos de PK foi criado
		PCICR |= (1<<PCIE2);
	}
}

PCInt::~PCInt(){}

void PCInt::set_f(uint8_t flag, uint8_t dado){
	if(flag == 0)	_flag0 |= dado;
	if(flag == 1)	_flag1 |= dado;
	if(flag == 2)	_flag2 |= dado;
}
/*
uint8_t PCInt::get_f(){return 1;}*/

void PCInt::enable(){ //habilita individualmente cada pino de interrupcao atraves da mascara
	if((_id >= 0) & (_id <= 7)){
		PCMSK0 |= (1<<this->tmp_id);
	}else if((_id >= 8) & (_id <= 15)){
		PCMSK1 |= (1<<this->tmp_id);
	}else if((_id >= 16) & (_id <= 23)){
		PCMSK2 |= (1<<this->tmp_id);
	}
}
void PCInt::disable(){//desabilita cada pino de interrupcao atraces da mascara
	if((_id >= 0) & (_id <= 7)){
		PCMSK0 &= ~(1<<this->tmp_id);
	}else if((_id >= 8) & (_id <= 15)){
		PCMSK1 &= ~(1<<this->tmp_id);
	}else if((_id >= 16) & (_id <= 23)){
		PCMSK2 &= ~(1<<this->tmp_id);
	}
}

void PCInt::manager_handler(){
	//veificar as portas PB
	if(_flag0 != 0){//verifica se interrupcao para esta porta esta habilitada
		for(int i = 0; i<=7;i++){
			if(PCMSK0 & (1<<i)){
				pcint_handler(0+i);
				_flag0 &= ~(1<<i);
			}
		}
	}
	//veificar as portas PJ
	if(_flag1 != 0){//verifica se interrupcao para esta porta esta habilitada
		for(int i = 0; i<=7;i++){
			if(PCMSK1 & (1<<i)){
				pcint_handler(8+i);
				_flag1 &= ~(1<<i);
			}
		}
	}
	//veificar as portas PK
	if(_flag2 != 0){//verifica se interrupcao para esta porta esta habilitada
		for(int i = 0; i<=7;i++){
			if(PCMSK2 & (1<<i)){
				pcint_handler(16+i);
				_flag2 &= ~(1<<i);
			}
		}
	}
}

ISR(PCINT0_vect){ PCInt::set_f(0, (PCMSK0 & PINB)); } //toda porta PB eh PCINT de 0 a 7;
ISR(PCINT1_vect){PCInt::set_f(1,((PINJ << 1)|(1 << 0)) & (0xFE | PINE)); }//PCInt::set_f(1, (PCMSK1 & (PINJ | (1<<PINE0))));}
				 // PCInt::set_f(1, (PCMSK1 & (1<<PINE0))); }//Apenas o PCINT8 esta associado ao pino PE0, o restante esta no PJ de 9 a 15
ISR(PCINT2_vect){ PCInt::set_f(2, (PCMSK2 & PINK)); }//toda porta PB eh pcint de 16 a 23
