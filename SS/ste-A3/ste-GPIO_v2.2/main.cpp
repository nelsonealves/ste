#include <avr/io.h>
#include <util/delay.h>
#include "GPIO.h"

GPIO botao0(3, GPIO::INPUT);
GPIO led1(10, GPIO::OUTPUT);

int main(void){
	//UCSR0B = 0;
	while (1){
		if (botao0.get()){
			led1.toggle();
			//_delay_ms(400);
		}
	}
	return 0;
}
