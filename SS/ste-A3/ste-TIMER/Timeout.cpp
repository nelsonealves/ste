/*
 * Timeout.cpp
 *
 *  Created on: 22 de out de 2018
 *      Author: guh86
 */
#include <stdlib.h>
#include "Timeout.h"

Timeout::Timeout(){
	this->_callback = 0;
	this->_counter = 0;
	this->_enable = false;
	this->_ev = false;
	this->_interval = 0;
}

Timeout::~Timeout(){}

int Timeout::get(){
	return this->_ev;
}

void Timeout::set(){
	this->_ev = false;
}

void Timeout::config(uint32_t _interval, CALLBACK_t _callback){
	this->_interval = _interval;
	this->_callback = _callback;
	this->_enable = true;
}

void Timeout::checkTimeout(){
	if (this->_enable == true){
		this->_counter++;
		if(this->_counter == this->_interval){
			this->_ev = true;
			this->_counter = 0;
		}
	}else{
		this->_counter = 0;
		this->_ev = false;
	}
}

void Timeout::Callback(){
	(*_callback)();
}
