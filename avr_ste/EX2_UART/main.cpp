#include <avr/io.h>
#include "util/delay.h"
#include <string.h>

#define FOSC 16000000// Clock Speed
#define BAUD 9600
#define MYUBRR (FOSC/16/BAUD-1)

void USART_Init( unsigned int ubrr) {
	UBRR0H = (unsigned char)(ubrr>>8);					//UBRRnH - Recebe 4 bits mais significantes
	UBRR0L = (unsigned char)ubrr;						//UBRRnL - 8 bits menos significantes
	UCSR0B = (1<<RXEN0)|(1<<TXEN0); 					// Habilita recepção e transmissão
	UCSR0C = (1<<USBS0)|(3<<UCSZ00); 					// Define formato do frame: 8 data-bit e 2 stop-bit
}

void USART_Transmit(unsigned char data) {
	while (!( UCSR0A & (1<<UDRE0))); 					// Bit que indica que está preparado para transmitir
	UDR0 = data; 										// Envia o data para o buffer
}

//void USART_print(char * data[]){
//	for(uint8_t i = 0; i < strlen(*data); i++){
//	        USART_Transmit(*data[i]);
//	    }
//}

unsigned char USART_Receive() {
	while (!(UCSR0A & (1<<RXC0)));				// Habilita interrupção pra leitura
	return UDR0;								// Get and return received data from buffer
}

int main(void) {
	USART_Init(MYUBRR);
	unsigned char data;

	while(1){
		data = USART_Receive();
		USART_Transmit(data);
	}

	return 0;
}



