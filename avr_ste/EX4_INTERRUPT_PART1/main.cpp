#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

ISR(INT0_vect){
	PORTE ^= (1 << PE5);
}

int main(void){
	DDRE |= 0xFF; 	// Setando DDH4 como saida PORTA 7
	DDRD &= ~(1 << DDD0);   // DDD0 como entrada PORTA 21
	PORTD |= (1 << PD0);
	PORTE &= ~(1 << PE5);
	UCSR0B = 0x00;
	EICRA = (1 << ISC01);
	EIMSK |= (1 << INT0); //habilita as duas interrupções
	sei(); //habilita interrupções globais, ativando o bit I do SREG
	while(1){}
	return 0;
}
//-----------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------
