/*
 * GPIO.h
 *
 *  Created on: 17 de set de 2018
 *      Author: nelson
 */
#include <avr/io.h>

#ifndef GPIO_H_
#define GPIO_H_




class GPIO {
public:
	 enum State{low=0, high};
	    enum Direction{in=0, out};
	    uint8_t * PIN[12][2] = {{    PE0,    PE1,    PE4,    PE5,    PG5,    PE3,    PH3,    PH4,    PH5,    PH6,    PB4,    PB5,    PB6},
	                        {    DDRE,     DDRE,     DDRE,     DDRE,     DDRG,     DDRE,     DDRH,     DDRH,     DDRH,     DDRH,     DDRB,     DDRB,     DDRB},
	                        {    PINE,     PINE,     PINE,    PINE,    PING,    PINE,    PINH,    PINH,    PINH,    PINH,    PINB,    PINB,    PINB}};
	    uint8_t * PORT[12] = {    PORTE,    PORTE,    PORTE,    PORTE,    PORTG,    PORTE,    PORTH,    PORTH,    PORTH,    PORTH,    PORTB,    PORTB,    PORTB};
	GPIO();
	GPIO(int pin, Direction dir);
	virtual ~GPIO();
	State get_state();
	void set_state(State state);
	void clear_state();
	void toogle_state();
	void pullup(State state);

private:
	int _id;
	uint8_t * _reg_pin;
	uint8_t * _reg_ddr;
	uint8_t * _reg_id;
	uint8_t * _reg_port;
	State * state;
};

#endif /* GPIO_H_ */
