
#include <avr/io.h>
#include <avr/interrupt.h>
#include <inttypes.h>


uint64_t _millis = 0;
uint16_t _1000us = 0;

uint64_t old_millis = 0;

/* interrupts routines */
// timer overflow occur every 0.256 ms
uint64_t millis() {
	uint64_t m;
	cli();
	m = _millis;
	sei();
	return m;
}
ISR(TIMER0_OVF_vect) {
	_1000us += 256;
	while (_1000us > 1000) {
		_millis++;
		_1000us -= 1000;
	}
}
ISR(INT0_vect){
	if ((millis() - old_millis) > 200) {
		PORTE ^= (1 << PE5);
		old_millis = millis();
	}
}


int main(void){
	DDRE |= (1 << DDE5); // set LED pin as output
	PORTE |= (1 << PE5); // turn the LED on
	DDRD &= ~(1 << DDD0);   // DDD0 como entrada PORTA 21
	PORTD |= (1 << PD0);
	UCSR0B = 0x00;
	/* interrupt setup */
	// prescale timer0 to 1/8th the clock rate
	// overflow timer0 every 0.256 ms
	TCCR0B |= (1 << CS01);
	// enable timer overflow interrupt
	TIMSK0  |= (1 << TOIE0);
	EICRA = (1 << ISC01);
	EIMSK |= (1 << INT0); // Habilita interrupção INT0
	sei();	//	Habilita interrupção global
	while(1){

	}
}
