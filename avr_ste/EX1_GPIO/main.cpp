/*
Botão 	- Porta 8 - PH5 - pIn
Led		- Porta 7 - PH4 - pOut
*/

#include <avr/io.h>

int main(void) {
	DDRH |= (1 << DDH5); 	// Setando DDH4 como saida
	DDRH &= ~(1 << DDH4); 	// Setando DDH5 como entrada
	PORTD |= (1 << PH4);

	while(true) {
		PINH & (1 << PH4) ? (PORTH |= (1 << PH5)) : (PORTH &= ~(1 << PH5));
	}

	return 0;
}

