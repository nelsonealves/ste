/*
 * main.cpp
 *
 *  Created on: 26 de set de 2018
 *      Author: guh86
 */
#include <avr/io.h>
#include <util/delay.h>
#include "UART.h"

UART serial1(9600, UART::databit_8, UART::none, UART::two, UART::slow);

int main(void){
	sei();
	uint8_t data = 0;
	char *frase = "Deu Certo --> ";
	while(true){
		while(serial1.hasdata()){
			data = serial1.dado_get();
			serial1.puts(frase);
			serial1.put(data);
		}

	}
}
