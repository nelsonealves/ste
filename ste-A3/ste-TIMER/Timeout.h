/*
 * Timeout.h
 *
 *  Created on: 22 de out de 2018
 *      Author: guh86
 */

#ifndef TIMEOUT_H_
#define TIMEOUT_H_

#include<stdint.h>
typedef void (* CALLBACK_t)(void);

//pode criar no maximo 4 timeouts
class Timeout {
public:
	Timeout();
	~Timeout();
	int get();
	void set();
	void config(uint32_t _interval, CALLBACK_t _callback);
	void checkTimeout();
	void Callback();

private:
	bool _ev; //tem evento para ser tratado ou nao
	bool _enable; //habilita timeout
	CALLBACK_t _callback; //armazena endereco do timeout
	uint32_t _counter; //contador de ticks do timeout
	uint32_t _interval; // intervalo da interrupcao
};

#endif /* TIMEOUT_H_ */
