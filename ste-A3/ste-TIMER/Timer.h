/*
 * Timer.h
 *
 *  Created on: 24 de mar de 2017
 *      Author: aluno
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <avr/interrupt.h>
#include "Singleton.h"
#include "Timeout.h"

typedef unsigned long Hertz;
typedef unsigned long long Microseconds;
typedef unsigned long long Milliseconds;

class Timer : public Singleton<Timer> {
public:
	/*
	 * Will configure timer to the closest frequency
	 * that does not produce rounding errors.
	 * Example:
	 *   freq	-	Actual Timer Frequency (Hz)
	 *   100	-	100,1602564
	 *   500	-	504,0322581
	 *   1000	-	1041,666667
	 *   2000	-	2232,142857
	 *   5000	-	5208,333333
	 *   10000	-	15625
	 */

	enum _type {
		TIMER_8 = 0,
		TIMER_16 = 1
	};

	Timer(_type bits, Hertz freq);

	Milliseconds millis();
	Microseconds micros();

	void delay(Milliseconds ms);
	void udelay(Microseconds us);
	static void ovf8_isr_handler();
	static void ovf16_isr_handler();
	void managerTimeout();
	void addTimeout(uint32_t interval, CALLBACK_t callback);

private:
	unsigned long long _ticks;
	unsigned int _timer_base;
	Microseconds _us_per_tick;
	Timeout timeouts[4];
	uint8_t _last;
};

#endif /* TIMER_H_ */
