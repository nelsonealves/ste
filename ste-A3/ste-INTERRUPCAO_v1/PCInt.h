/*
 * PCInt.h
 *
 *  Created on: 5 de nov de 2018
 *      Author: guh86
 */

#ifndef PCINT_H_
#define PCINT_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>

typedef void (* toFuncao_t)(void);

class PCInt {
public:

enum int_id{PCINT_0=0, PCINT_1=1, PCINT_2=2, PCINT_3=3, PCINT_4=4, PCINT_5=5, PCINT_6=6, PCINT_7=7,
			PCINT_8=8, PCINT_9=9, PCINT_10=10, PCINT_11=11, PCINT_12=12, PCINT_13=13, PCINT_14=14, PCINT_15=15,
			PCINT_16=16, PCINT_17=17, PCINT_18=18, PCINT_19=19, PCINT_20=20, PCINT_21=21, PCINT_22=22, PCINT_23=23
		};

	PCInt(int_id _id, toFuncao_t pCallback);
	~PCInt();
	void enable();
	void disable();
	static void set_f(uint8_t flag, uint8_t data);
	uint8_t get_f();
	static void manager_handler();
	static void pcint_handler(uint8_t pos){ (PCInt::_pCallback)[pos](); }

private:
	uint8_t _id;
	uint8_t tmp_id;
	static uint8_t _flag0, _flag1, _flag2;
	static toFuncao_t _pCallback[24];


};
#endif /* PCINT_H_ */
