/*
 * extInt.h
 *
 *  Created on: 3 de out de 2018
 *      Author: guh86
 */

#ifndef EXTINT_H_
#define EXTINT_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>

typedef void (* toFuncao_t)(void);

class extInt {
	public:
		enum int_id{
			INT_0 = 0, INT_1 = 1, INT_2 = 2, INT_3 = 3, INT_4 = 4, INT_5 = 5, INT_6 = 6, INT_7 = 7
		};

		enum isc_t{
			low = 0, 	// 00
			any = 1, 	// 01
			falling = 2,// 10
			rising = 3, // 11
		};

		extInt(int_id _id, isc_t _int_config, toFuncao_t pCallback);
		~extInt();
		void enable();
		void disable();
		static void int0_handler(){ (*_pCallback_0)(); }
		static void int1_handler(){ (*_pCallback_1)(); }
		static void int2_handler(){ (*_pCallback_2)(); }
		static void int3_handler(){ (*_pCallback_3)(); }
		static void int4_handler(){ (*_pCallback_4)(); }
		static void int5_handler(){ (*_pCallback_5)(); }
		static void int6_handler(){ (*_pCallback_6)(); }
		static void int7_handler(){ (*_pCallback_7)(); }
	private:
		uint8_t _id;
		static toFuncao_t _pCallback_0;
		static toFuncao_t _pCallback_1;
		static toFuncao_t _pCallback_2;
		static toFuncao_t _pCallback_3;
		static toFuncao_t _pCallback_4;
		static toFuncao_t _pCallback_5;
		static toFuncao_t _pCallback_6;
		static toFuncao_t _pCallback_7;

};
#endif /* EXTINT_H_ */
