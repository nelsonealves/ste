/*
 * Discador.h
 *
 *  Created on: 28 de nov de 2018
 *      Author: nelson
 */

#ifndef DISCADOR_H_
#define DISCADOR_H_

#include "GPIO.h"
#include <avr/io.h>
#include <util/delay.h>


class Discador {
	public:
		Discador(uint8_t l1, uint8_t l2 , uint8_t l3,uint8_t l4, uint8_t c1, uint8_t c2, uint8_t c3);
		~Discador();
		void discar(char numero);
	private:
		GPIO linha1;
		GPIO linha2;
		GPIO linha3;
		GPIO linha4;
		GPIO coluna1;
		GPIO coluna2;
		GPIO coluna3;

};

#endif /* DISCADOR_H_ */
