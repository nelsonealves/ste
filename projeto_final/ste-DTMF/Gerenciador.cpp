/*
 * Gerenciador.cpp
 *
 *  Created on: 4 de dez de 2018
 *      Author: nelson
 */

#include "Gerenciador.h"

Gerenciador::Gerenciador(uint8_t prog, uint8_t start, uint8_t cancel, Discador * discador) : serial(9600, UART::databit_8, UART::none, UART::two, UART::slow),
prog(prog, GPIO::INPUT), start(start, GPIO::INPUT), cancel(cancel, GPIO::INPUT) {
	state = idle;
	enviar = discador;
	cont = 0;
	sei();
}

Gerenciador::~Gerenciador() {}

bool Gerenciador::check(char dado){
	if		(dado == '1') return true;
	else if (dado == '2') return true;
	else if (dado == '3') return true;
	else if (dado == '4') return true;
	else if (dado == '5') return true;
	else if (dado == '6') return true;
	else if (dado == '7') return true;
	else if (dado == '8') return true;
	else if (dado == '9') return true;
	else if (dado == '0') return true;
	else if (dado == '*') return true;
	else if (dado == '#') return true;
	else return false;

}

void Gerenciador::insert_custom(){
	uint8_t data_serial;
	if(serial.hasdata()){
		data_serial = serial.dado_get();
		if(check(data_serial)){
			dado[cont] = data_serial;
			serial.puts(" Caracter adicionado: ");
			serial.put(data_serial);
			serial.puts("\n");
			cont++;
		}else serial.puts(" Caracter invalido...");
	}
}

bool Gerenciador::send(const char * c){
	while(*c){
		if(!(cancel.get())) return false;
		enviar->discar(*c);
		c++;
	}
	return true;
}

void Gerenciador::gerenciador(){
//	_delay_ms(1000);
	serial.puts("\nGerenciador STE 2018/2\n");
	serial.puts("Botao P - Programacao customizada \n");
	serial.puts("Botao S - Programacao padrao\n");
	while(true){
		switch(state){
			case idle:
				if (!(prog.get())) {
					serial.puts("\nInicializando programacao...\n");
					memset(dado, 0, sizeof(dado));
					cont = 0;
					_delay_ms(500);
					serial.puts("Aperte prog novamente ao terminar...\n");
					serial.puts("\nEnvie os Caracteres...\n");
					state = custom;
				}
				if (!(start.get()))	state = standart;
				if (!(cancel.get())) state = idle;
				break;
			case standart:
				dado[0] = '1'; dado[1] = '2'; dado[2] = '3'; dado[3] = '4';
				dado[4] = '5'; dado[5] = '6'; dado[6] = '7'; dado[7] = '8';
				dado[8] = '9'; dado[9] = '*'; dado[10] = '0'; dado[11] = '#';
				serial.puts("\nDefault carregada...\n");
				state = test;
				break;
			case custom:
				if(cont < 15) insert_custom();
				else{
					serial.puts("\nLimite do buffer atingido...\n");
					_delay_ms(500);
					state = wait;
				}
				if (!(cancel.get())){
					serial.puts("\nCancelando Customizacao...\n");
					_delay_ms(500);
					state = idle;
				}
				if (!(prog.get())){
					serial.puts("\nCustomizacao carregada...\n");
					_delay_ms(500);
					state = wait;
				}
				break;
			case test:
				serial.puts("\nEnviando...\n");
				send(dado);
				if (!(cancel.get())){
					serial.puts("\nCancelando envio...\n");
					_delay_ms(500);
					state = wait;
				}
				else state = wait;
				break;
			case wait:
				if (!(start.get())) state = test;
				if (!(cancel.get())){
					serial.puts("\nGerenciador STE 2018/2\n");
					serial.puts("Botao P - Programacao customizada \n");
					serial.puts("Botao S - Programacao padrao\n");
					_delay_ms(500);
					state = idle;
				}
				break;
		}
	}
}
