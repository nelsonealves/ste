/*
 * Gerenciador.h
 *
 *  Created on: 4 de dez de 2018
 *      Author: nelson
 */

#ifndef GERENCIADOR_H_
#define GERENCIADOR_H_

#include "Discador.h"
#include "GPIO.h"
#include "UART.h"
#include <util/delay.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>



class Gerenciador {
	public:
		enum State{idle=0, wait, standart, custom, test} state;
		Gerenciador(uint8_t prog, uint8_t start, uint8_t cancel, Discador * enviar);
		~Gerenciador();
		bool check(char dado);
		void insert_custom();
		void gerenciador();
		bool send(const char * c);
	private:
		UART serial;
		GPIO prog, start, cancel;
		Discador * enviar;
		char dado[15];
		int cont;
};

#endif /* GERENCIADOR_H_ */
