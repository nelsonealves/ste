/*
 * Discador.cpp
 *
 *  Created on: 28 de nov de 2018
 *      Author: nelson
 */

#include "Discador.h"


Discador::Discador(uint8_t l1, uint8_t l2 , uint8_t l3,uint8_t l4, uint8_t c1, uint8_t c2, uint8_t c3) :
linha1(l1, GPIO::OUTPUT), linha2(l2, GPIO::OUTPUT), linha3(l3, GPIO::OUTPUT), linha4(l4, GPIO::OUTPUT),
coluna1(c1, GPIO::OUTPUT), coluna2(c2, GPIO::OUTPUT), coluna3(c3, GPIO::OUTPUT) {



}

Discador::~Discador() {

}

void Discador::discar(char numero){
	GPIO * linha;
	GPIO * coluna;

	switch(numero){
		case '1': case '2': case '3':
			linha = &linha1;
			break;
		case '4': case '5': case '6':
			linha = &linha2;
			break;
		case '7': case '8': case '9':
			linha = &linha3;
			break;
		case '*': case '0': case '#':
			linha = &linha4;
			break;
	}
	switch(numero){
		case '1': case '4': case '7': case '*':
			coluna = &coluna1;
			break;
		case '2': case '5': case '8': case '0':
			coluna = &coluna2;
			break;
		case '3': case '6': case '9': case '#':
			coluna = &coluna3;
			break;
	}

	linha->set(true);
	coluna->set(true);
	_delay_ms(1000);
	linha->set(false);
	coluna->set(false);
}


