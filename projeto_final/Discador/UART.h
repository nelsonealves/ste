#ifndef UART_H_
#define UART_H_


#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>
#include "Singleton.h"
#include "FIFO.h"

class UART : public Singleton<UART> {
public:
	//enum para mascaramento
			enum _databit{
					databit_5 = 0, //'00000000'
					databit_6 = 2, //'00000010
					databit_7 = 4, //'00000100'
					databit_8 = 6, //'00000110'
				};

			enum _parity{
					none = 0, //'000000000'
					even = 32, //'00010000'
					odd = 48, //'00110000'
				};

			enum _stopbit{
					one = 0, //'00000000'
					two = 8, //'00001000'
				};

			enum _speed{
					slow = 0, //'00000000'
					fast = 2, //'00000010'
				};

	UART(uint16_t baud, uint8_t _databit, uint8_t _parity, uint8_t _stopbit, uint8_t _speed);
	~UART();
	void put(uint8_t data); //transmissor
	void puts(const char * c); //imprime char
	int dado_get (); 	  	//receptor
	bool hasdata();		 	//verifica se tem dado ou nao
	static void rx_ISR(); 	//trata a interrupcao de recepcao
	static void tx_ISR(); 	//trata a interrupcao de transmissao

private:
	FIFO<uint8_t, 64>tx;
	FIFO<uint8_t, 64>rx;
	//bool _newdata;
	//uint8_t _rxbuffer;
	//uint8_t _txbuffer;

};

#endif /* UART_H_ */
