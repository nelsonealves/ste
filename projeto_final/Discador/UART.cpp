/*
 * UART.cpp
 *
 *  Created on: 26 de set de 2018
 *      Author: guh86
 */

#include "UART.h"

UART::UART(uint16_t baud, uint8_t _databit, uint8_t _parity, uint8_t _stopbit, uint8_t _speed) {
	if( _speed == UART::slow) UBRR0 = F_CPU/16/baud-1; //configura o baudrate
	else UBRR0 = F_CPU/8/baud-1;
	UCSR0B = _BV(RXEN0)|_BV(TXEN0);			//habilita tx e rx do registrador B
	UCSR0C |= _databit | _parity | _stopbit;//define formato do frame com 8 bits e 2 stop bits
	UCSR0B |= _BV(RXCIE0);
	UCSR0A |= _speed; //verificar se precisa????
	//_newdata = false;
	//_rxbuffer = 0;
	//_txbuffer = 0;
}

UART::~UART() {
}

void UART::put(uint8_t data){ //transmissor
	while(self()->tx.cheia());
	self()->tx.push(data);
	//self()->_txbuffer = data;
	UCSR0B |= _BV(UDRIE0);
}

void UART::puts(const char * c){
	while(*c){
		put(*c);
		c++;
	}
}

int UART::dado_get (){ //receptor
	//self()->_newdata = false;
	return self()->rx.pop();
}

bool UART::hasdata(){
	if(self()->rx.get_size() != 0) return true;
	else return false;
	//return self()->_newdata;
}

ISR(USART0_RX_vect){UART::rx_ISR();}
void UART::rx_ISR(){
	self()->rx.push(UDR0); // o push ja trata tamanho da fila;
	//self()->_rxbuffer = UDR0;
	//self()->_newdata = true;
}

ISR(USART0_UDRE_vect){UART::tx_ISR();}
void UART::tx_ISR(){
	UDR0 = self()->tx.pop(); // pega o que a
	//UDR0 = self()->_txbuffer;
	if(self()->tx.vazia())
		UCSR0B &= ~(1<<UDRIE0);
}
