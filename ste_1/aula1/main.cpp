#include <avr/io.h>

void led_on(){
	PORTB |= 1 << DDB7;
}
void led_off(){
	PORTB &= ~(1 << DDB7);
}

int main(){
	DDRB |= 1 << DDB7;
	DDRB &= ~(1 << DDB6);
	while(1){

		if(PINB & 1 << DDB7){
			led_on();
		}else{
			led_off();
		}
	}

	return 0;

}
