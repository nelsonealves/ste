/*
 * main.cpp
 *
 *  Created on: 22 de out de 2018
 *      Author: guh86
 */

#include <avr/io.h>
//#include <util/delay.h>
#include "Timer.h"
#include "GPIO.h"

//TIMER_8 ou TIMER_16
Timer timer1(Timer::TIMER_16, 2000);

GPIO led1(9, GPIO::OUTPUT);
GPIO led2(10, GPIO::OUTPUT);
GPIO led3(11, GPIO::OUTPUT);
GPIO led4(12, GPIO::OUTPUT);

void pisca1(void){
	led1.toggle();
}

void pisca2(void){
	led2.toggle();
}

void pisca3(void){
	led3.toggle();
}

void pisca4(void){
	led4.toggle();
}

int main(void){
	timer1.addTimeout(50, &pisca1);
	timer1.addTimeout(100, &pisca2);
	timer1.addTimeout(50, &pisca3);
	timer1.addTimeout(100, &pisca4);
	sei();
	while(true){
		timer1.managerTimeout();
	}
	return 0;
}
