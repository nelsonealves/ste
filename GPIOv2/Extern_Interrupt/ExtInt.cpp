/*
 * ExtInt.cpp
 *
 *  Created on: 3 de out de 2018
 *      Author: nelson
 */



#include "ExtInt.h"


namespace Interrupt{

to_func  ExtInt::p_callback[8];

ExtInt::ExtInt(uint8_t id, uint8_t isc, to_func callback , bool enable_it){
	_id = id;

	switch(id){
		case 0:
			EICRA |= (1 << ISC01) | (isc << ISC00);
			break;
		case 1:
			EICRA |= (1 << ISC11) | (isc << ISC10);
			break;
		case 2:
			EICRA |= (1 << ISC21) | (isc << ISC20);
			break;
		case 3:
			EICRA |= (1 << ISC31) | (isc << ISC30);
			break;
		case 4:
			EICRB |= (1 << ISC41) | (isc << ISC40);
			break;
		case 5:
			EICRB |= (1 << ISC51) | (isc << ISC50);
			break;
		case 6:
			EICRB |= (1 << ISC61) | (isc << ISC60);
			break;
		case 7:
			EICRB |= (1 << ISC71) | (isc << ISC70);
			break;
	}
	p_callback[_id] = callback;
	if(enable_it) enable();
}



void ExtInt::enable(){
	EIMSK |= (1 << _id);
}

void ExtInt::disable(){
	EIMSK &= (1 << _id);
}


ISR(INT0_vect){
	ExtInt::vect_handler(0);
}
ISR(INT1_vect){
	ExtInt::vect_handler(1);
}
ISR(INT2_vect){
	ExtInt::vect_handler(2);
}
ISR(INT3_vect){
	ExtInt::vect_handler(3);
}
ISR(INT4_vect){
	ExtInt::vect_handler(4);
}
ISR(INT5_vect){
	ExtInt::vect_handler(5);
}
ISR(INT6_vect){
	ExtInt::vect_handler(6);
}
ISR(INT7_vect){
	ExtInt::vect_handler(7);
}
}



