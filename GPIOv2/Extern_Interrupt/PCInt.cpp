/*
 * PCInt.cpp
 *
 *  Created on: 10 de nov de 2018
 *      Author: nelson
 */

#include "PCInt.h"


to_func  PCInt::p_callback[24];

PCInt::PCInt() {


}

PCInt::~PCInt() {
	// TODO Auto-generated destructor stub
}

void PCInt::attachInterrupt(int id, to_func callback){
	int bit = id % 8;
	if(id <= 7 ){
		PCICR |= (1 << 0);
		PCMSK0 |= (1 << bit);
	} else if( id <= 15) {
		PCICR |= (1 << 1);
		PCMSK0 |= (1 << bit);
	} else if( id <= 23) {
		PCICR |= (1 << 2);
		PCMSK0 |= (1 << bit);
	}

	p_callback[id] = callback;
}

void PCInt::detachInterrupt(int id){

}

void PCInt::find_change(volatile uint8_t * pcmsk){
	uint8_t bit;
	uint8_t pin;

	for (uint8_t i=0; i < 8; i++) {
	    bit = 0x01 << i;
	    if (bit & *pcmsk) {

	      //if (p_callback[pin] != NULL) {
	        p_callback[pin]();
	      //}
	    }
	  }
}


ISR(PCINT0_vect){
	PCInt::find_change(port_to_mask[0]); //PB
}
ISR(PCINT1_vect){
	PCInt::find_change(port_to_mask[1]); //PJ
}
ISR(PCINT2_vect){
	PCInt::find_change(port_to_mask[2]); //PK
}


