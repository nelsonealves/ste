/*
 * ExtInt.h
 *
 *  Created on: 3 de out de 2018
 *      Author: nelson
 */

#ifndef EXTINT_H_
#define EXTINT_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include <stdint.h>

typedef void(*to_func)(void);

namespace Interrupt  {
	class ExtInt {
		public:
			enum ISC_t {LOW=0, ANY, FALLING, RISING};
			enum INTx {INT_0=0, INT_1, INT_2, INT_3, INT_4, INT_5, INT_6, INT_7};
			ExtInt(uint8_t id, uint8_t isc, to_func callback , bool enable_it);
			~ExtInt(){};
			void enable();
			void disable();
			static void vect_handler(uint8_t vect){ (ExtInt::p_callback[vect])();}
		private:
			static to_func  p_callback[8];
			uint8_t _id;


	};
}
#endif /* EXTINT_H_ */


