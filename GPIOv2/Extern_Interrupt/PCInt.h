/*
 * PCInt.h
 *
 *  Created on: 10 de nov de 2018
 *      Author: nelson
 */

#ifndef PCINT_H_
#define PCINT_H_
#include "GPIO_Port.h"
#include <avr/interrupt.h>

typedef void(*to_func)(void);


volatile uint8_t * port_to_mask[] = {&PCMSK0, &PCMSK1, &PCMSK2};

class PCInt {
	public:
		PCInt();
		~PCInt();
		void attachInterrupt(int id, to_func callback);
		void detachInterrupt(int pin);
		static void vect_handler(uint8_t vect){ (PCInt::p_callback[vect])();}
		static void find_change(volatile uint8_t * pcmsk);

	private:
		static to_func  p_callback[24];
		static uint8_t pcmsk_temp[3];
};



#endif /* PCINT_H_ */
