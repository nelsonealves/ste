#include <avr/interrupt.h>
#include "ExtInt.h"
#include "UART.h"
#include "GPIO.h"

using namespace Interrupt;

UART uart(19200, UART::databit_8, UART::none, UART::one);
GPIO gpio(43, GPIO::INPUT);

void int0_handler(void){
	uart.put('0');
	return;
}

void int1_handler(void){
	uart.put('1');
	return;
}

void int2_handler(void){
	uart.put('2');
	return;
}

void int3_handler(void){
	uart.put('3');
	return;
}

void int4_handler(void){
	uart.put('4');
	return;
}

void int5_handler(void){
	uart.put('5');
	return;
}

void int6_handler(void){
	uart.put('6');
	return;
}

void int7_handler(void){
	uart.put('7');
	return;
}

int main( void ){

	ExtInt int0_obj(ExtInt::INT_0, ExtInt::FALLING, &int1_handler, false);
	UART uart(9600, UART::databit_8, UART::none, UART::two);
	sei();
	int0_obj.enable();

	while(1){

	}

	return 0;
}
