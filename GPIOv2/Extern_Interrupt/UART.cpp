

#include "UART.h"

UART::UART(uint16_t baud, uint8_t _databit, uint8_t _parity, uint8_t _stopbit) {
	UBRR0 = F_CPU/16/baud-1;				//configura o baudrate
	UCSR0B = _BV(RXEN0)|_BV(TXEN0);			//habilita tx e rx do registrador B
	UCSR0C |= _databit | _parity | _stopbit;//define formato do frame com 8 bits e 2 stop bits
	UCSR0B |= _BV(RXCIE0);
}

UART::~UART() {
}

void UART::puts(char string[]){
	for(int i =0; string[i]>0; i++)
			put(string[i]);

}

void UART::put(uint8_t data){
//	while(!(UCSR0A & (1<<UDRE0))); 		//UCSR0A controle do registrador A , UDRE0 flag para saber se o buffer ta vazio
//		UDR0 = data;					//buffer do registrador 0
	self()->FIFO_TX.push(data);
	UCSR0B |= _BV(UDRIE0);
}

uint8_t UART::dado_get (){
//	if (self()->FIFO_RX.vazia()) return NULL;
//
//	return self()->FIFO_RX.pop();
return (self()->FIFO_RX.vazia()) ? 0 :  self()->FIFO_RX.pop();
}

bool UART::hasdata(){
	return self()->FIFO_RX.get_size();
}

ISR(USART0_RX_vect){UART::rx_ISR();}
void UART::rx_ISR(){
	self()->FIFO_RX.push(UDR0);
}

ISR(USART0_UDRE_vect){UART::tx_ISR();}
void UART::tx_ISR(){
	UDR0 = self()->FIFO_TX.pop();
	UCSR0B &= ~(1<<UDRIE0);
}
