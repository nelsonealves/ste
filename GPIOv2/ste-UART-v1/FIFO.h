#ifndef FIFO_H_
#define FIFO_H_

template <typename TYPE, uint8_t SIZE> class FIFO {

public:
	FIFO() {
		_in = _out = _last = 0;
	}

	bool push(TYPE data){
		if (_last >= SIZE){
			return true;
		}
		_buffer[_in] = data;
		_in = ((_in+1) % SIZE);
		_last ++; //incrementa a quantidade de itens na FIFO
		return false;
	}

	TYPE pop(){
		if(_last == 0) return (TYPE)true;
		else{
			TYPE data = _buffer[_out];
			_out = (_out+1) % SIZE;
			_last --;
			return data;
		}
	}

	int get_size(){
		return _last;
	}

	void clear(){
		_in = _out = _last = 0;
	}

	bool cheia(){
		return (_last == SIZE);
	}

	bool vazia(){
		return(_last == 0);
	}

private:
	TYPE _buffer[SIZE];
	uint8_t _in, _out, _last; //inicio, fim e quantidade de itens da lista
};

#endif /* FIFO_H_ */
