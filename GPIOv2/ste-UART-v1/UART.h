
#ifndef UART_H_
#define UART_H_
#define NULL "\0"
#include "Singleton.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include "FIFO.h"
#include <string.h>

class UART : public Singleton<UART> {
public:
//			enum mode {
//				NORMAL=1, // 16
//				DOUBLE=2,
//			};

			enum _databit{
					databit_5 = 0, //'00000000'
					databit_6 = 2, //'00000010
					databit_7 = 4, //'00000100'
					databit_8 = 6, //'00000110'
				};

			enum _parity{
					none = 0, //'000000000'
					even = 32, //'00010000'
					odd = 48, //'00110000'
				};

			enum _stopbit{
					one = 0, //'00000000'
					two = 8, //'00001000'
				};

	UART(uint16_t baud, uint8_t _databit, uint8_t _parity, uint8_t _stopbit);
	~UART();
	void put(uint8_t data); //transmissor
	uint8_t dado_get (); 	  	//receptor
	bool hasdata();		 	//verifica se tem dado ou nao
	void puts(char string[]);
	static void rx_ISR(); 	//trata a interrupcao de recepcao
	static void tx_ISR(); 	//trata a interrupcao de transmissao

private:
	FIFO<uint8_t, 64> FIFO_TX;
	FIFO<uint8_t, 64> FIFO_RX;
};

#endif /* UART_H_ */
