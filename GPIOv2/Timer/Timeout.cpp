/*
 * Timeout.cpp
 *
 *  Created on: 22 de out de 2018
 *      Author: nelson
 */

#include "Timeout.h"

Timeout::Timeout() {
	_counter = 0;
	_event = false;
	_enable = false;
	interval = 0;
	_callback = 0;
}

Timeout::~Timeout() {

}

void Timeout::config(uint32_t interval_t, to_func callback_t) {
	_enable = true;
	interval = interval_t;
	_callback = callback_t;
}

void Timeout::checkTimeout() {
	if (_enable) {
		_counter++;
		if (_counter == interval) {
			_event = true;
			_counter = 0;
		}
	}

}

void Timeout::callback() {
	(*_callback)();
	_event = false;
}
