/*
 * Timeout.h
 *
 *  Created on: 22 de out de 2018
 *      Author: nelson
 */

#ifndef TIMEOUT_H_
#define TIMEOUT_H_

#include <avr/interrupt.h>
#include <stdint.h>

typedef void (* to_func)(void);

class Timeout {
public:
	Timeout();
	~Timeout();
	void config(uint32_t interval, to_func callback_t);
	void checkTimeout();
	void callback();
	bool get_event(){return _event;}
	bool get_enable(){return _enable;}
	void set_event(bool event){ _event = event;}
	uint32_t interval;
private:
	bool _event;
	bool _enable;
	uint32_t _counter;
	to_func _callback;
};

#endif /* TIMEOUT_H_ */
