#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "Timer.h"
#include "GPIO.h"


GPIO gpio1(2, GPIO::OUTPUT);
GPIO gpio2(3, GPIO::OUTPUT);
GPIO gpio3(4, GPIO::OUTPUT);
GPIO gpio4(5, GPIO::OUTPUT);


void func_test1(){
	gpio1.toggle();
}

void func_test2(){
	gpio2.toggle();
}
void func_test3(){
	gpio3.toggle();
}
void func_test4(){
	gpio4.toggle();
}


int main(void){
	sei();
	Timer timer(1000);
	timer.addTimeout(1000, &func_test1);
	timer.addTimeout(500, &func_test2);
	timer.addTimeout(200, &func_test3);
	timer.addTimeout(50, &func_test4);

	while(1){
		timer.timeoutManager();
	}
	return 0;
}
