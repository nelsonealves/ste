/*
 * GPIO.cpp
 *
 *  Created on: 22 de mar de 2017
 *      Author: aluno
 */
//verificar se eh 8 ou 16bits quando usar progmem
//verificar se continua usando const
//verificar se pode declarar direto no array ou individualmente para depois montar o array

#include "GPIO.h"

GPIO::GPIO(uint8_t id, PortDirection_t dir)
{
	_bit = _port->ler_bit_flash(id);
	_port = GPIO_PORT::AllPorts[_port->ler_port_flash(id)];
	_port->dir(_bit, dir);
}

GPIO::~GPIO() {}

bool GPIO::get() {
	return _port->get(_bit);
}

void GPIO::set(bool val) {
	_port->set(_bit, val);
}

void GPIO::clear() {
	this->set(0);
}

void GPIO::toggle() {
	_port->toggle(_bit);
}
