################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Discador.cpp \
../GPIO.cpp \
../GPIO_Port.cpp \
../Gerenciador.cpp \
../UART.cpp \
../main.cpp 

OBJS += \
./Discador.o \
./GPIO.o \
./GPIO_Port.o \
./Gerenciador.o \
./UART.o \
./main.o 

CPP_DEPS += \
./Discador.d \
./GPIO.d \
./GPIO_Port.d \
./Gerenciador.d \
./UART.d \
./main.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: AVR C++ Compiler'
	avr-g++ -Wall -g2 -gstabs -O0 -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -funsigned-char -funsigned-bitfields -fno-exceptions -mmcu=atmega2560 -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


